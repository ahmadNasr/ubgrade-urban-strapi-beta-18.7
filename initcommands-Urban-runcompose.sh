#!/bin/bash

mongo --host mongo --eval 'db.createUser({user:"myUserAdmin",pwd:"abc123",roles:[{role:"userAdminAnyDatabase",db:"admin"}],mechanisms:["SCRAM-SHA-1"]})'

echo "installing packages backend project Strapi"
npm install

echo "installing packages Urban"
cd client && npm install

echo "build Strapi admin"
npm run build

RUN echo "Running UI and Strapi projects"
npm run dev
