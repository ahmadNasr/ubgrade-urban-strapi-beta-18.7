'use strict';

/**
 * City.js controller
 *
 * @description: A set of functions called "actions" for managing `City`.
 */

module.exports = {
  getCityFactSheetByCity: async ctx => {
    return await City.findOne({
      pCode: ctx.params.pcode
    })
      .select('pCode')
      .populate({
        path: 'products',
        select: 'type createdAt document language',
        match: { type: { $eq: 'City_Fact_Sheet' }, language: { $ne: null } },
        options: {
          sort: { createdAt: -1 }
        },
        populate: [{ path: 'document', select: 'url' }]
      })
      .then(city => {
        if (!city || city.products === undefined || city.products.length == 0) {
          return ctx.notFound();
        }

        let product = { en: null, ar: null };
        city.products
          .filter(
            (product, index, self) =>
              self.findIndex(t => t.language === product.language) === index
          )
          .forEach(e => {
            e.language === 'en' && (product.en = e.document.url);
            e.language === 'ar' && (product.ar = e.document.url);
          });

        return product;
      })
      .catch(err => {
        return err;
      });
  },
  getCityProfileReportByCity: async ctx => {
    return await City.findOne({
      pCode: ctx.params.pcode
    })
      .select('pCode')
      .populate({
        path: 'products',
        select: 'type createdAt document',
        match: { type: { $eq: 'City_Profile_Report' } },
        options: {
          limit: 1,
          sort: { createdAt: -1 }
        },
        populate: [{ path: 'document' }]
      })
      .then(city => {
        if (!city || city.products === undefined || city.products.length == 0) {
          return ctx.notFound();
        }
        return city.products;
      })
      .catch(err => {
        return err;
      });
  },
  getCityProfileReportSkeletonByCity: async ctx => {
    return await City.findOne({
      pCode: ctx.params.pcode
    })
      .select('pCode')
      .populate({
        path: 'products',
        select: 'type createdAt document',
        match: { type: { $eq: 'City_Profile_Report' } },
        options: {
          limit: 1,
          sort: { createdAt: -1 }
        },
        populate: [{ path: 'document' }]
      })
      .then(city => {
        if (!city || city.products === undefined || city.products.length == 0) {
          return ctx.notFound();
        }
        return true;
      })
      .catch(err => {
        return err;
      });
  },
  getInteractiveCityProfileByCity: async ctx => {
    return await City.findOne({
      pCode: ctx.params.pcode
    })
      .select('pCode')
      .populate({
        path: 'products',
        select: 'type createdAt document name',
        match: { type: { $eq: 'Interactive_City_Profile' } },
        options: {
          limit: 1,
          sort: { createdAt: -1 }
        },
        populate: [{ path: 'document' }]
      })
      .then(city => {
        if (!city || city.products === undefined || city.products.length == 0) {
          return ctx.notFound();
        }
        return city.products;
      })
      .catch(err => {
        return err;
      });
  },
  getInteractiveCityProfileSkeletonByCity: async ctx => {
    return await City.findOne({
      pCode: ctx.params.pcode
    })
      .select('pCode')
      .populate({
        path: 'products',
        select: 'type createdAt document name',
        match: { type: { $eq: 'Interactive_City_Profile' } },
        options: {
          limit: 1,
          sort: { createdAt: -1 }
        },
        populate: [{ path: 'document' }]
      })
      .then(city => {
        if (!city || city.products === undefined || city.products.length == 0) {
          return ctx.notFound();
        }
        return true;
      })
      .catch(err => {
        return err;
      });
  },

  getDataSetsByCity: async ctx => {
    return await City.findOne({
      pCode: ctx.params.pcode
    })
      .select('pCode')
      .populate({
        path: 'products',
        select: 'type createdAt document name',
        match: { type: { $eq: 'Datasets' } },
        options: {
          limit: 1,
          sort: { createdAt: -1 }
        },
        populate: [{ path: 'document' }]
      })
      .then(city => {
        if (!city || city.products === undefined || city.products.length == 0) {
          return ctx.notFound();
        }
        return city.products;
      })
      .catch(err => {
        return err;
      });
  },
  getDataSetsSkeletonByCity: async ctx => {
    return await City.findOne({
      pCode: ctx.params.pcode
    })
      .select('pCode')
      .populate({
        path: 'products',
        select: 'type createdAt document name',
        match: { type: { $eq: 'Datasets' } },
        options: {
          limit: 1,
          sort: { createdAt: -1 }
        },
        populate: [{ path: 'document' }]
      })
      .then(city => {
        if (!city || city.products === undefined || city.products.length == 0) {
          return ctx.notFound();
        }
        return true;
      })
      .catch(err => {
        return err;
      });
  },
  getUrbanBaselineByCity: async ctx => {
    return await City.findOne({
      pCode: ctx.params.pcode
    })
      .select('pCode')
      .populate({
        path: 'products',
        select: 'type createdAt document language',
        match: { type: { $eq: 'Urban_Baseline' }, language: { $ne: null } },
        options: {
          sort: { createdAt: -1 }
        },
        populate: [{ path: 'document', select: 'url' }]
      })
      .then(city => {
        if (!city || city.products === undefined || city.products.length == 0) {
          return ctx.notFound();
        }

        let product = { en: null, ar: null };
        city.products
          .filter(
            (product, index, self) =>
              self.findIndex(t => t.language === product.language) === index
          )
          .forEach(e => {
            e.language === 'en' && (product.en = e.document.url);
            e.language === 'ar' && (product.ar = e.document.url);
          });

        return product;
      })
      .catch(err => {
        return err;
      });
  }
};
