import ReactGa from 'react-ga';
import i18n from 'i18next';

// Names Actions

export const INIT_GA = 'INIT_REACT_GA';

export const INIT_ENGLISH = 'INIT_ENGLISH';

export const INIT_ARABIC = 'INIT_ARABIC';

export const IS_ENGLISH = 'IS_ENGLISH';

export const IS_ARABIC = 'IS_ARABIC';

export const IS_CHANGED_TO_ENGLISH = 'IS_CHANGED_TO_ENGLISH';

export const IS_CHANGED_TO_ARABIC = 'IS_CHANGED_TO_ARABIC';

export const SEND_PAGE_VIEW = 'SEND_PAGE_VIEW';

export const SEND_PAGE_VIEW_CHECK_CHANGED_LANGUAGE = 'SEND_PAGE_VIEW_CHECK_CHANGED_LANGUAGE';

export const SEND_PAGE_VIEW_CHECK_CHANGED_LANGUAGE_ARABIC = 'SEND_PAGE_VIEW_CHECK_CHANGED_LANGUAGE_ARABIC';

export const SEND_GOOGLE_ANALYTICS_EVENT = 'SEND_GOOGLE_ANALYTICS_EVENT';

export const SEND_EVENT_ENGLISH = 'SEND_EVENT_ENGLISH';

export const SEND_EVENT_ARABIC = 'SEND_EVENT_ARABIC';


// Actions


var Actions = {
    initGaAction: function () {
        Actions.initEnglishAction.call(this);
        Actions.initArabicAction.call(this);
    },


    initEnglishAction: function () {
        if (Actions.isEnglishAction(this)) {
            ReactGa.initialize(this.trackingIdEnglish);
            this.isEnglishInit = true;
        }

    },
    initArabicAction: function () {
        if (Actions.isArabicAction(this)) {
            ReactGa.ga('create', this.trackingIdArabic, 'auto', 'arabicTracker');
            this.isArabicInit = true;
        }
    },

    isEnglishAction: function () {
        let currentLangauge = i18n.language || 'en';
        // let isEnglish = currentLangauge === 'en' && this.previousLang !== 'en';
        let isEnglish = currentLangauge === 'en';
        if (isEnglish) {
            this.previousLang = 'en';
        }

        return isEnglish;
    },

    isArabicAction: function () {
        let currentLangauge = i18n.language || 'en';
        // let isArabic = currentLangauge === 'ar' && this.previousLang !== 'ar';
        let isArabic = currentLangauge === 'ar';
        if (isArabic) {
            this.previousLang = 'ar';
        }

        return isArabic;
    },

    isChangedToEnglishAction: function () {
        let currentLangauge = i18n.language || 'en';
        let isEnglish = (currentLangauge === 'en' && this.previousLang !== 'en');
        if (isEnglish) {
            this.previousLang = 'en';
        }

        return isEnglish;
    },

    isChangedToArabicAction: function () {
        let currentLangauge = i18n.language || 'en';
        let isArabic = (currentLangauge === 'ar' && this.previousLang !== 'ar');
        if (isArabic) {
            this.previousLang = 'ar';
        }

        return isArabic;
    },

    sendPageViewAction: function () {
        Actions.initGaAction.call(this);
        console.log('send page view called')

        if (Actions.isEnglishAction.call(this)) {
            ReactGa.pageview('/');
        } else if (Actions.isArabicAction.call(this)) {
            ReactGa.ga('arabicTracker.send', 'pageview');
        }
    },

    sendPageViewCheckChangedLanguageAction: function () {
        Actions.initGaAction.call(this);
        console.log('send page view called')

        if (Actions.isChangedToEnglishAction.call(this)) {
            ReactGa.pageview('/');
        } else if (Actions.isChangedToArabicAction.call(this)) {
            ReactGa.ga('arabicTracker.send', 'pageview');
        }
    },

    sendPageViewCheckChangedLanguageArabicAction: function () {
        Actions.initGaAction.call(this);
        console.log('send page view called')

        if (Actions.isChangedToArabicAction.call(this)) {
            ReactGa.ga('arabicTracker.send', 'pageview');
        }
    },

    sendGoogleAnalyticsEventAction: function (categoryGa, actionGa) {
        Actions.initGaAction.call(this);

        if (Actions.isEnglishAction.call(this)) {
            Actions.sendEventEnglishAction.call(this, categoryGa, actionGa);
        } else if (Actions.isArabicAction.call(this)) {
            Actions.sendEventArabicAction.call(this, categoryGa, actionGa);
        }
    },

    sendEventEnglishAction: function (category, action) {
        Actions.initGaAction.call(this);

        ReactGa.event({
            category: category,
            action: action
        });
    },

    sendEventArabicAction: function (category, action) {
        Actions.initGaAction.call(this);

        ReactGa.ga('arabicTracker.send', {
            hitType: 'event',
            eventCategory: category,
            eventAction: action
        });
    },
}


// Actions creator

// function buyCake() {
//     return {
//         type: BUY_CAKE,
//         info: 'First redux action'
//     }
// }

function initGa() {
    return {
        type: INIT_GA,
        info: 'init React Google Analytics for current selected language (English, Arabic) tracking',
        action: Actions.initGaAction
    }
}

function initEnglish() {
    return {
        type: INIT_ENGLISH,
        info: 'init React Google Analytics for English tracking',
        action: Actions.initEnglishAction
    }
}

function initArabic() {
    return {
        type: INIT_ARABIC,
        info: 'init React Google Analytics for Arabic tracking',
        action: Actions.initArabicAction
    }
}

function isEnglish() {
    return {
        type: IS_ENGLISH,
        info: 'check if the current language is English',
        action: Actions.isEnglishAction
    }
}

function isArabic() {
    return {
        type: IS_ARABIC,
        info: 'check if the current language is Arabic',
        action: Actions.isArabicAction
    }
}

export function isChangedToEnglish() {
    return {
        type: IS_CHANGED_TO_ENGLISH,
        info: 'check if the current language is changed to English tracking',
        action: Actions.isChangedToEnglishAction
    }
}

export function isChangedToArabic() {
    return {
        type: IS_CHANGED_TO_ARABIC,
        info: 'check if the current language is changed to Arabic tracking',
        action: Actions.isChangedToArabicAction
    }
}

export function sendPageView() {
    return {
        type: SEND_PAGE_VIEW,
        info: "send page view with link '/' to current selected language tracking",
        action: Actions.sendPageViewAction
    }
}

export function sendPageViewCheckChangedLanguage() {
    return {
        type: SEND_PAGE_VIEW_CHECK_CHANGED_LANGUAGE,
        info: "send page view with link '/' to current selected language tracking with checking if the language is changed",
        action: Actions.sendPageViewCheckChangedLanguageAction
    }
}

export function sendPageViewCheckChangedLanguageArabic() {
    return {
        type: SEND_PAGE_VIEW_CHECK_CHANGED_LANGUAGE_ARABIC,
        info: "send page view with link '/' to current selected language tracking with checking if the language is changed",
        action: Actions.sendPageViewCheckChangedLanguageArabicAction
    }
}

export function sendGoogleAnalyticsEvent(categoryGa, actionGa) {
    return {
        type: SEND_GOOGLE_ANALYTICS_EVENT,
        info: "send event to current selected language tracking",
        action: Actions.sendGoogleAnalyticsEventAction,

        params: {
            categoryGa: categoryGa,
            actionGa: actionGa
        }
    }
}

function sendEventEnglish() {
    return {
        type: SEND_EVENT_ENGLISH,
        info: "send event to English tracking",
        action: Actions.sendEventEnglishAction
    }
}

function sendEventArabic() {
    return {
        type: SEND_EVENT_ARABIC,
        info: "send event to Arabic tracking",
        action: Actions.sendEventArabicAction
    }
}