const isEmpty = value =>
  value === undefined ||
  value === null ||
  value === '' ||
  (typeof value === 'object' && Object.keys(value).length === 0) ||
  (typeof value === 'string' && value.trim().length === 0) ||
  // Object of null values
  (typeof value === 'object' &&
    Object.values(value).every(element => element === null)) ||
  // Array of null
  (Array.isArray(value) && value.every(e => e === null)) ||
  // Array of Arrays of null
  (Array.isArray(value) &&
    value.every(e => Array.isArray(e) && e.every(el => el === null))) ||
  // Array of objects of null values
  (Array.isArray(value) &&
    value.every(
      e => typeof e === 'object' && Object.values(e).every(el => el === null)
    ));

export default isEmpty;
