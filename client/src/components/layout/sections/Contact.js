import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import { Container, Row, Col, Button } from 'mdbreact';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import axios from 'axios';
import toPersianDigits from '../../../utils/toPersianDigits';

class Contact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      message: '',
      success: false,
      loading: false,
      errors: {}
    };
  }

  onChange = e => {
    this.setState({
      ...this.state,
      [e.target.name]: e.target.value,
      success: false,
      errors: { ...this.state.errors, [e.target.name]: '' }
    });
  };

  onSubmit = e => {
    e.preventDefault();

    this.setState({ loading: true, success: false });

    const contactData = {
      name: this.state.name,
      email: this.state.email,
      message: this.state.message
    };

    axios
      .post(`${process.env.REACT_APP_API_URL}/auth/contact`, contactData)
      .then(res =>
        this.setState({ success: res.data, loading: false, errors: {} })
      )
      .catch(err =>
        this.setState({
          errors: err.response.data.message,
          success: false,
          loading: false
        })
      );
  };

  render() {
    const { errors, success, loading } = this.state;
    const { t } = this.props;
    return (
      <Container
        fluid
        className={`${!t('language.isRTL') ? 'text-left' : 'text-right'}`}
        dir={`${t('language.isRTL') && 'rtl'}`}
      >
        <Row center className="urban-elegant-gray-4 z-depth-2">
          <Col
            md="6"
            size="12"
            className="d-flex align-items-center justify-content-start"
          >
            <div className="text-white m-5 px-4 w-100">
              <h2 className="h2-responsive font-weight-bold pb-2 text-uppercase">
                {t('contact.title')}
              </h2>

              {success && (
                <div className="alert alert-success font-smaller">
                  {t('forms.contact-success')}
                </div>
              )}
              <form noValidate onSubmit={this.onSubmit}>
                <label className="font-small">{t('forms.name')}</label>
                <input
                  type="text"
                  className={`form-control form-control-sm ${errors.name &&
                    'is-invalid'}`}
                  name="name"
                  value={this.state.name}
                  onChange={this.onChange}
                />

                {errors.name && (
                  <div className="invalid-feedback">
                    {t(`forms.${errors.name}`)}
                  </div>
                )}
                <br />

                <label className="font-small">{t('forms.email')}</label>
                <input
                  type="email"
                  className={`form-control form-control-sm ${errors.email &&
                    'is-invalid'}`}
                  name="email"
                  value={this.state.email}
                  onChange={this.onChange}
                />

                {errors.email && (
                  <div className="invalid-feedback">
                    {t(`forms.${errors.email}`)}
                  </div>
                )}
                <br />

                <label className="font-small">{t('forms.msg')}</label>
                <textarea
                  type="text"
                  className={`form-control form-control-sm ${errors.message &&
                    'is-invalid'}`}
                  name="message"
                  value={this.state.message}
                  onChange={this.onChange}
                  rows="5"
                />

                {errors.message && (
                  <div className="invalid-feedback">
                    {t(`forms.${errors.message}`)}
                  </div>
                )}

                <div
                  className={`my-2 ${
                    !t('language.isRTL') ? 'text-right' : 'text-left'
                  }`}
                >
                  <Button
                    type="submit"
                    className={`btn waves-effect waves-light btn-rounded-urban-dark-orange font-weight-600 btn-sm mx-0 ${loading &&
                      'disabled'}`}
                  >
                    {loading ? (
                      <div
                        className="spinner-grow text-white spinner-grow-sm"
                        role="status"
                      >
                        <span className="sr-only">{t('forms.loading')}</span>
                      </div>
                    ) : (
                      <span>{t('forms.send')}</span>
                    )}
                  </Button>
                </div>
              </form>
            </div>
          </Col>
          <Col
            md="6"
            size="12"
            className="d-flex align-items-center justify-content-start"
          >
            <div className="text-white px-4 m-5">
              <h4 className="h4-responsive font-weight-normal pb-2 text-uppercase">
                {t('contact.copyrights.title')}
              </h4>

              <p className="font-small m-0">
                ©{' '}
                {!t('language.isRTL')
                  ? new Date().getFullYear()
                  : toPersianDigits(new Date().getFullYear().toString())}{' '}
                {t('contact.copyrights.body')}
              </p>
            </div>
          </Col>
        </Row>
      </Container>
    );
  }
}

Contact.propTypes = {
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  errors: state.errors
});

export default connect(
  mapStateToProps,
  {}
)(withTranslation()(Contact));
