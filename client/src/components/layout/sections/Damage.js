import {
  Col,
  Container,
  Row,
  toast
} from 'mdbreact';
import React, { Component } from 'react';

import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { show } from 'redux-modal';
import { withTranslation } from 'react-i18next';

const damageAssessmentMapSeries =
  'https://urban-syria.org/proxy/esri/storymap-series/damage-assessment';

class Damage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalDamage: false,
      products: [],
      productsAr: []
    };
  }

  toggle = () => {
    this.setState({
      modalDamage: !this.state.modalDamage
    });
  };

  handleOpen = name => () => {
    toast.dismiss();
    this.props.show(name);
  };

  render() {
    const { t } = this.props;

    return (
      <Container
        fluid
        className={`${t('language.isRTL') && 'text-right'}`}
        dir={`${t('language.isRTL') && 'rtl'}`}>
        <Row center className='urban-dark-orange z-depth-2'>
          <Col lg='7' md='8' sm='9' size='10'>
            <div className='text-white my-5 mx-2'>
              <div className='d-flex flex-row align-items-center justify-content-center pb-3 mb-2'>
                <h2 className='h2-responsive mb-0 text-center font-weight-bold'>
                  {t('damage.damage-assess.paragraph.title')}
                </h2>
                <div className='px-2 badge-container'>
                  <span className='badge badge-warning urban-darker-blue-opacity'>
                    {t('damage.damage-assess.paragraph.badge')}
                  </span>
                </div>
              </div>

              <p className='mb-0'>
                {t('damage.damage-assess.paragraph.body.p1')}{' '}
                {localStorage.jwtToken ? (
                  <span>
                    {t('damage.damage-assess.paragraph.body.p5')}{' '}
                    <u
                      className='text-urban-dark-blue cursor-pointer'
                      onClick={this.toggle}>
                      {t('damage.damage-assess.paragraph.body.p6')}
                    </u>{' '}
                  </span>
                ) : (
                    <span>
                      {t('damage.damage-assess.paragraph.body.p2')}{' '}
                      <u
                        className='text-urban-dark-blue cursor-pointer'
                        onClick={this.handleOpen('authModal')}>
                        {t('damage.damage-assess.paragraph.body.p3')}
                      </u>{' '}
                      {t('damage.damage-assess.paragraph.body.p4')}
                    </span>
                  )}
              </p>
            </div>
          </Col>
        </Row>
      </Container>
    );
  }
}

Damage.propTypes = {
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, { show })(withTranslation()(Damage));
