import React from 'react';
import {
  Container,
  Modal,
  ModalFooter,
  Row,
  Fa,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  MDBBtn
} from 'mdbreact';
import Login from './Login';
import Register from './Register';
import { withTranslation } from 'react-i18next';
import {
  show as showMethod,
  hide as hideMethod,
  connectModal
} from 'redux-modal';
import { connect } from 'react-redux';

class AuthModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeItem: '1'
    };
  }

  handleOpen = name => () => {
    this.props.showMethod(name);
    this.props.hideMethod('authModal');
  };

  toggleTab = tab => () => {
    if (this.state.activeItem !== tab) {
      this.setState({
        activeItem: tab
      });
    }
  };

  render() {
    const { show, handleHide, t } = this.props;

    return (
      <Container
        className={`font-weight-normal ${
          !t('language.isRTL') ? 'text-left' : 'text-right'
        }`}
        dir={`${t('language.isRTL') && 'rtl'}`}
      >
        <Row>
          <Modal
            className="form-cascading"
            isOpen={show}
            toggle={handleHide}
            centered
          >
            <Nav
              tabs
              className="md-tabs nav-justified tabs-2 light-blue darken-3 font-smaller-1 p-2"
              style={{ margin: '-1.5rem 1rem 0 1rem' }}
            >
              <NavItem>
                <NavLink
                  className={
                    this.state.activeItem === '1' ? 'active py-2' : 'py-2'
                  }
                  to="#"
                  onClick={this.toggleTab('1')}
                  role="tab"
                  replace
                >
                  <Fa icon="user" className="fa-sm mx-2" />
                  {t('forms.login')}
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={
                    this.state.activeItem === '2' ? 'active py-2' : 'py-2'
                  }
                  to="#"
                  onClick={this.toggleTab('2')}
                  role="tab"
                  replace
                >
                  <Fa icon="user-plus" className="fa-sm mx-2" />
                  {t('forms.register')}
                </NavLink>
              </NavItem>
            </Nav>
            <TabContent
              className="card bs-none pt-3"
              activeItem={this.state.activeItem}
            >
              <TabPane tabId="1" role="tabpanel">
                <Login />
                <ModalFooter className="justify-content-center align-items-center mx-3 pb-2">
                  <Row className="w-100 justify-content-between align-items-center bt-0">
                    <div>
                      <p className="font-smaller-2 grey-text mb-0">
                        {t('forms.not-member')}{' '}
                        <span
                          className="blue-text cursor-pointer"
                          onClick={this.toggleTab('2')}
                        >
                          {t('forms.sign-up')}
                        </span>
                      </p>
                      <p className="font-smaller-2 grey-text mb-0">
                        {t('forms.help-i')}{' '}
                        <span
                          className="blue-text cursor-pointer"
                          onClick={this.handleOpen('forgotPasswordModal')}
                        >
                          {t('forms.forgot-my-pwd')}
                        </span>
                      </p>
                    </div>
                    <MDBBtn
                      className="font-weight-500"
                      outline
                      color="info"
                      size="sm"
                      onClick={handleHide}
                    >
                      {t('forms.close')}
                    </MDBBtn>
                  </Row>
                </ModalFooter>
              </TabPane>
              <TabPane tabId="2" role="tabpanel">
                <Register />
                <ModalFooter className="justify-content-center align-items-center mx-3 pb-2">
                  <Row
                    className="w-100 justify-content-between align-items-center bt-0"
                    style={{ borderTop: '1px solid #e9ecef' }}
                  >
                    <div>
                      <p className="font-smaller-2 grey-text mb-0">
                        {t('forms.have-account')}
                        <span
                          className="blue-text cursor-pointer ml-1"
                          onClick={this.toggleTab('1')}
                        >
                          {t('forms.login')}
                        </span>
                      </p>
                    </div>
                    <MDBBtn
                      className="font-weight-500"
                      outline
                      color="info"
                      size="sm"
                      onClick={handleHide}
                    >
                      {t('forms.close')}
                    </MDBBtn>
                  </Row>
                </ModalFooter>
              </TabPane>
            </TabContent>
          </Modal>
        </Row>
      </Container>
    );
  }
}

export default connectModal({ name: 'authModal' })(
  connect(
    null,
    { showMethod, hideMethod }
  )(withTranslation()(AuthModal))
);
