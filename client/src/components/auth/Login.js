import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import { Button, ModalBody } from 'mdbreact';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { loginUser } from '../../actions/authActions';
import { connectModal } from 'redux-modal';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      identifier: '',
      password: '',
      success: false,
      loading: false,
      errors: {}
    };
  }

  componentDidUpdate(prevProps) {
    if (this.props !== prevProps) {
      if (this.props.errors.login === null) {
        this.setState({ errors: {}, loading: false, success: true });
      }
      if (this.props.errors.login && this.props.errors.login.message) {
        this.setState({
          errors: this.props.errors.login.message,
          success: false,
          loading: false
        });
      }
    }
  }

  onChange = e => {
    this.setState({
      ...this.state,
      [e.target.name]: e.target.value,
      success: false,
      errors: { ...this.state.errors, [e.target.name]: '' }
    });
  };

  onSubmit = e => {
    e.preventDefault();
    this.setState({ loading: true, success: false });
    const userData = {
      identifier: this.state.identifier.toLowerCase(),
      password: this.state.password
    };
    this.props.loginUser(userData);
  };

  render() {
    const { errors, loading } = this.state;
    const { t } = this.props;

    return (
      <ModalBody className='mx-3 p-0 px-2 pt-2 pb-3'>
        <form noValidate onSubmit={this.onSubmit}>
          <label className='grey-text font-smaller-1'>{t(`forms.email`)}</label>
          <input
            type='email'
            className={`form-control form-control-sm text-lowercase ${errors.identifier &&
              'is-invalid'}`}
            name='identifier'
            value={this.state.identifier.toLowerCase()}
            onChange={this.onChange}
          />

          {errors.identifier && (
            <div className='invalid-feedback'>
              {t(`forms.${errors.identifier}`)}
            </div>
          )}

          <hr className='pb-3 m-0 bt-0' />

          <label className='grey-text font-smaller-1'>{t('forms.pwd')}</label>
          <input
            type='password'
            className={`form-control form-control-sm ${errors.password &&
              'is-invalid'}`}
            name='password'
            value={this.state.password}
            onChange={this.onChange}
          />

          {errors.password && (
            <div className='invalid-feedback'>
              {t(`forms.${errors.password}`)}
            </div>
          )}

          <Button
            type='submit'
            className={`font-weight-500 mt-4 mb-0 flex-center mx-auto ${loading &&
              'disabled'}`}
            color='info'
            size='sm'>
            {loading ? (
              <div
                className='spinner-grow text-white spinner-grow-sm'
                role='status'>
                <span className='sr-only'>{t('forms.loading')}</span>
              </div>
            ) : (
              <span>{t('forms.login')}</span>
            )}
          </Button>
        </form>
      </ModalBody>
    );
  }
}

Login.propTypes = {
  loginUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connectModal({ name: 'authModal' })(
  connect(mapStateToProps, { loginUser })(withRouter(withTranslation()(Login)))
);
