const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateResetPasswordInput(data) {
  let errors = {};

  data.password = !isEmpty(data.password) ? data.password : '';
  data.passwordConf = !isEmpty(data.passwordConf) ? data.passwordConf : '';

  if (!Validator.isLength(data.password, { min: 6, max: 30 }))
    errors.password = 'Password must be at least 6 characters';

  if (strapi.api.user.services.user.isHashed(data.password))
    errors.password = "Password can't contain the $ symbol more than twice";

  if (!Validator.isLength(data.passwordConf, { min: 6, max: 30 }))
    errors.passwordConf = 'Password confirmation must be at least 6 characters';

  if (!Validator.equals(data.password, data.passwordConf))
    errors.passwordConf = 'Passwords must match';

  if (Validator.isEmpty(data.password))
    errors.password = 'Password field is required';

  if (Validator.isEmpty(data.passwordConf))
    errors.passwordConf = 'Password confirmation field is required';

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
